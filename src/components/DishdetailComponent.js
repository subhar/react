import React, { Component } from 'react';
import { Card, CardImg, CardText, CardBody,
    CardTitle, Breadcrumb, BreadcrumbItem, Button, Label, Col, Row, Modal, ModalHeader, ModalBody } from 'reactstrap';
import { Link } from 'react-router-dom';
import { Control, LocalForm, Errors } from 'react-redux-form';

class CommentForm extends Component {
		constructor (props) {
		super(props);
		this.state = {
			isModalOpen: false
		}
		this.toggleModal = this.toggleModal.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	toggleModal() {
		this.setState({
			isModalOpen: !this.state.isModalOpen
		});
	}

    handleSubmit(values) {
        console.log("Current state is: " + JSON.stringify(values));
        alert("Current state is: " + JSON.stringify(values));
    }

    render() {
    	return(
    		<React.Fragment>
    		
    			<Button outline onClick={this.toggleModal} bsSize="large">
    			<span className="fa fa-pencil fa-lg"></span> Submit Comment
    			</Button>
    			<Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
		        	<ModalHeader toggle={this.toggleModal}>Submit Comment</ModalHeader>
		        	<ModalBody>
                        <div className="col-12">
                            <LocalForm onSubmit={(values) => this.handleSubmit(values)}>
                                <Row className="form-group">
                                    <Label htmlFor="rating" md={2}><strong>Rating</strong></Label>
                                    <Col md={{size: 12}}>
                                        <Control.select model=".rating" id="rating" name="rating"
                                            className="form-control">
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                                            <option>6</option>
                                         </Control.select>
                                    </Col>
                                </Row>

                             	<Row className="form-group">
                                    <Label htmlFor="author" md={4}><strong>Your Name</strong></Label>
                                    <Col md={{size: 12}}>
                                        <Control.text model=".author" id="author" name="author"
                                            placeholder="Your Name"
                                            className="form-control" 
                                            validators= {{
                                                required, minLength: minLength(3), maxLength: maxLength(15)
                                            }}/>
                                        <Errors className="text-danger"
                                            model=".author"
                                            show="touched"
                                            messages={{
                                                required: 'Required',
                                                minLength: 'Must be greater than 2 characters',
                                                maxLength: "Must be 15 characters or less"
                                            }}
                                        />
                                    </Col>
                                </Row>

                                <Row className="form-group">
                                    <Label htmlFor="comment" md={4}><strong>Comment</strong></Label>
                                    <Col md={{size: 12}}>
                                        <Control.textarea model=".comment" id="comment" name="comment"
                                            rows="6"
                                            className="form-control" />
                                    </Col>
                                </Row>
                                <Row className="form-group">
                                    <Col md={{size:10}}>
                                        <Button type="submit" color="primary">
                                            Submit
                                        </Button>
                                    </Col>
                                </Row>
                            </LocalForm>
                         </div>
		        	</ModalBody>
		        </Modal>
    		</React.Fragment>
    	);

    }
}

function RenderDish({dish}) {
	if (dish != null) {
		return(
			<Card>
				<CardImg width="100%" src={dish.image} alt={dish.name} />
				<CardBody>
					<CardTitle>{dish.name}</CardTitle>
					<CardText>{dish.description}</CardText>
				</CardBody>
			</Card>
			);
	} else {
		return (
			<div></div>
		);
	}
}

function RenderComments({comments}) {
	if (comments != null) {
		return (
		<div>
		<ul className="list-unstyled">
			{comments.map((comment) => {
				return (
					<li key={comment.id}>
						<div>{comment.comment}</div>
						<br/>
						<div>-- {comment.author}, {new Intl.DateTimeFormat('en-US', {year: 'numeric', month: 'short', day:'2-digit'}).format(new Date(Date.parse(comment.date)))}</div>
						<br/>
					</li>
				);
			})}
		</ul>

		<CommentForm /></div>);
	} else {
		return (<div></div>);
	}
}

const DishDetail = (props) => {
		
		 if (props.dish != null) {
		 	return (<div className="container"> 
			 <div className="row">
				<Breadcrumb>
				<BreadcrumbItem><Link to="/menu">Menu</Link></BreadcrumbItem>
					<BreadcrumbItem active>{props.dish.name}</BreadcrumbItem>
				</Breadcrumb>
					<div className="col-12">
					<h3>{props.dish.name}</h3>
					<hr />
				</div>                
			</div>
			 <div className="row">
		          <div  className="col-12 col-md-5 m-1">
		            <RenderDish dish={props.dish} />
		          </div>
		          <div className="col-12 col-md-5 m-1">
		           <h4>Comments</h4>
		           <RenderComments comments={props.comments} />
				  </div>
			  </div>
		</div>);

	} else { 
        return(
        	<div></div>
        );
    }
}

const required = (val) => val && val.length;
const maxLength = (len) => (val) => !(val) || (val.length <= len);
const minLength = (len) => (val) => (val) && (val.length >= len);



export default DishDetail;
